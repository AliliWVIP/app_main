// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';


String convertToUnderline(String input) {
  StringBuffer result = StringBuffer();
  for (int i = 0; i < input.length; i++) {
    if (i > 0 && input[i].toUpperCase() == input[i]) {
      result.write('_');
    }
    result.write(input[i].toLowerCase());
  }
  return result.toString();
}

List<dynamic> listConvert(List<dynamic> list) {
  List<dynamic> newList = list;
  for (int i = 0; i < list.length; i++) {
    dynamic ob = list[i];
    if (ob is List<dynamic>) {
      newList[i] = listConvert(ob);
      continue;
    }
    if (ob is Map<String, dynamic>) {
      newList[i] = mapConvert(ob);
    }
  }
  return newList;
}

Map<String, dynamic> mapConvert(Map<String, dynamic> map){
  Map<String, dynamic> newMap = {};
  map.forEach((key, value) { // 遍历map
    final newKey = convertToUnderline(key);
    if (value is Map<String, dynamic>) {
      value = mapConvert(value);
      // print("value === $value");
    }
    if (value is List<dynamic>) {
      value = listConvert(value);
    }
    newMap[newKey] = value;
  });
  return newMap;
}

void main() {


  void test() {
    // {"aB":{"aBc":1},"ABC":[{"aBc":"aBc"},"ABC"]}
    final map = {"aB":{"aBc":1},"ABC":[{"aBc":"aBc"},"ABC"]};
    final newMap = mapConvert(map);
    print(newMap);
  }


  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    test();
    // Build our app and trigger a frame.
    // await tester.pumpWidget(const MyApp());
    //
    // // Verify that our counter starts at 0.
    // expect(find.text('0'), findsOneWidget);
    // expect(find.text('1'), findsNothing);
    //
    // // Tap the '+' icon and trigger a frame.
    // await tester.tap(find.byIcon(Icons.add));
    // await tester.pump();
    //
    // // Verify that our counter has incremented.
    // expect(find.text('0'), findsNothing);
    // expect(find.text('1'), findsOneWidget);
    // test();
  });





}

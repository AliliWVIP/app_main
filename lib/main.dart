import 'package:app_main/min_max_queue.dart';
import 'package:flutter/material.dart';

import 'image_provider.dart';
import 'my_math.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;


  // orignal 图片中的原始点
  // url 图片地址
  // react大小
  Future<Offset> tranform(Offset original, String url, Size size) async {
    // 获取图片的宽高
    ImageInfo imageInfo = await SyncNetworkImage(url:url).getImage();
    int imageHeight = imageInfo.image.height;
    int imageWidth = imageInfo.image.width;
    // 矩形框
    double reactWidth = size.width;
    double reactHeight = size.height;
    // 图片中的x y
    double x = original.dx;
    double y = original.dy;

    // 判断充满高还是宽 /*** 这里先按照宽处理, 图片显示的宽等于矩形宽
    // 图片与矩形的宽高分别做比例，比例小的为所需要的缩放

    // 计算缩放比例
    double sX = reactWidth / imageWidth;
    double sY = reactHeight / imageHeight;
    double scale = sX < sY ? sX : sY;

    // 计算缩放后图片的起点
    double originX = (reactWidth - imageWidth * scale) / 2;
    double originY = (reactHeight - imageHeight * scale) / 2;


    // 计算相对的坐标
    double scaleX = getNeedDouble(x * scale + originX);
    double scaleY = getNeedDouble(y * scale + originY);

    print("x == $scaleX y == $scaleY");

    return Offset(scaleX, scaleY);

  }
  
  double getNeedDouble(double num){
    String numString = num.toStringAsFixed(2);
    return double.parse(numString);
  }


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          InkWell(
            onTap: (){
              Map result = MyMath().mapConvert({"aB":{"aBc":1},"ABC":[{"aBc":"aBc"},"ABC"]});
              print("result === $result");
              // List<int> result = Interview().twoSum([1, 3, 5, 7], 8);
              // print("result === $result");
            },
            child: Container(
                alignment: Alignment.center,
                height: 50,
                child: const Text("key驼峰法转为下划线")),
          ),
          InkWell(
            onTap: (){
              var inputStr = 'hellowolrd, {"key1":{"key2":[{"key3":"a","key4":[1,2,3]}]},"key5":"b"}hello,{"key6":1}}world!{d{s,{"id":1}}}["a"]';

              // var inputStr = 'hellowolrd, {"key5":"b"}hello["a"]';
              // 截取JSON对象
              var output = MyMath().extractJsonObjects(inputStr);
              print(output);

            },
            child: Container(
                alignment: Alignment.center,
                height: 50,
                child: const Text("给定一个字符串，截取其中包含的所有json object，用数组返回")),
          ),
          InkWell(
            onTap: (){
              // 测试代码
              var queue = MinMaxQueue();
              queue.push(3);
              queue.push(2);
              print(queue.queryMin());  // 输出: 2
              queue.push(1);
              print(queue.queryMax());  // 输出: 3
              print(queue.queryMin());  // 输出: 1
              print(queue.popFront());
              print(queue.queryMax());  // 输出: 3
            },
            child: Container(
                alignment: Alignment.center,
                height: 50,
                child: const Text("设计一个可以O(1)查询最大最小值的队列 (push,pop_front 操作也均为O1)")),
          ),
          InkWell(
            onTap: (){
              // 测试代码
              String result =MyMath().solve(["joker", "joker", "2", "2", "k", "k", "j", "j"], ['2', '2', '10', '10', '6', '6', '6', '6']);
              print(result);
            },
            child: Container(
                alignment: Alignment.center,
                height: 50,
                child: const Text("设计一个算法，解决斗地主残局问题")),
          )
        ],
      ),
    );
  }
}

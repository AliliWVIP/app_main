import 'dart:async';

import 'package:flutter/material.dart';

abstract class SyncImageProvider {
  Completer<ImageInfo> _completer = Completer();

  Future<ImageInfo> getImage() {
    try {
      _loadImage();
    } catch (e) {
      _completer.completeError(e);
    }
    return _completer.future;
  }

  _loadImage();
}

class SyncNetworkImage extends SyncImageProvider {
  String url;

  SyncNetworkImage({required this.url});

  @override
  void _loadImage() {
    var resolve = NetworkImage(url).resolve(ImageConfiguration.empty);
    resolve.addListener(ImageStreamListener((image, synchronousCall) {
      if (_completer.isCompleted) {
        return;
      }
      _completer.complete(image);
    }));
  }
}
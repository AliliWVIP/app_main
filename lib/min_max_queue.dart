import 'dart:collection';

class MinMaxQueue {
  Queue<int> _queue = Queue();
  Queue<int> _maxQueue = Queue();
  Queue<int> _minQueue = Queue();

  void push(int value) {
    _queue.add(value);
    while (_maxQueue.isNotEmpty && _maxQueue.last < value) {
      _maxQueue.removeLast();
    }
    _maxQueue.add(value);
    while (_minQueue.isNotEmpty && _minQueue.last > value) {
      _minQueue.removeLast();
    }
    _minQueue.add(value);
  }

  int? popFront() {
    if (_queue.isEmpty) return null;
    int value = _queue.removeFirst();
    if (value == _maxQueue.first) {
      _maxQueue.removeFirst();
    }
    if (value == _minQueue.first) {
      _minQueue.removeFirst();
    }
    return value;
  }

  int? queryMin() {
    return _minQueue.isNotEmpty ? _minQueue.first : null;
  }

  int? queryMax() {
    return _maxQueue.isNotEmpty ? _maxQueue.first : null;
  }
}
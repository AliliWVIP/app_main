import 'dart:convert';
import 'dart:core';

class MyMath {

  String convertToUnderline(String input) {
    StringBuffer result = StringBuffer();
    for (int i = 0; i < input.length; i++) {
      if (i > 0 && input[i].toUpperCase() == input[i]) {
        result.write('_');
      }
      result.write(input[i].toLowerCase());
    }
    return result.toString();
  }

  List<dynamic> listConvert(List<dynamic> list) {
    List<dynamic> newList = list;
    for (int i = 0; i < list.length; i++) {
      dynamic ob = list[i];
      if (ob is List<dynamic>) {
        newList[i] = listConvert(ob);
        continue;
      }
      if (ob is Map<String, dynamic>) {
        newList[i] = mapConvert(ob);
      }
    }
    return newList;
  }

  Map<String, dynamic> mapConvert(Map<String, dynamic> map){
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) { // 遍历map
      final newKey = convertToUnderline(key);
      if (value is Map<String, dynamic>) {
        value = mapConvert(value);
      }
      if (value is List<dynamic>) {
        value = listConvert(value);
      }
      newMap[newKey] = value;
    });
    return newMap;
  }

  // 解析JSON
  List<dynamic> extractJsonObjects(String input) {
    //  结果列表
    final List<dynamic> jsonObjects = [];
    int index = 0;
    while (index < input.length) {
      final currentChar = input[index];
      if (currentChar == '{' || currentChar == '[') {
        // 如果当前字符是{或[,表示开始解析JSON对象或数组
        final endIndex = findMatchingClosingBracket(input, index);
        // 找到与之匹配的}或]的索引
        if (endIndex != -1) {
          final jsonString = input.substring(index, endIndex + 1);
          // 提取JSON字符串
          try {
            final jsonObject = jsonDecode(jsonString);
            // 解析JSON字符串
            jsonObjects.add(jsonObject);
          } catch (e) {
            // 如果解析失败,说明是无效的JSON,跳过这个字符
          }
          index = endIndex + 1;
        } else {
          // 如果没有找到匹配的},说明JSON字符串无效,跳过这个字符
          index++;
        }
      } else {
        index++; // 其他字符,直接跳过
      }
    }
    return jsonObjects;
  }

  int findMatchingClosingBracket(String input, int startIndex) {
    final openingBracket = input[startIndex]; // 起始字符,{ 或 [
    final closingBracket = getClosingBracket(openingBracket); // 找出与之匹配的 } 或 ]
    int bracketCount = 1; // 括号数量初始化为1
    int index = startIndex + 1; // 从起始字符的下一个开始找
    while (index < input.length) {
      final currentChar = input[index];
      if (currentChar == openingBracket) {
        // 如果是相同的{,增加括号数量
        bracketCount++;
      } else if (currentChar == closingBracket) {
        // 如果是},减少括号数量
        bracketCount--;
        if (bracketCount == 0) { // 如果数量变为0,表示找到了匹配的}
          return index;
        }
      } else if (currentChar == '{' || currentChar == '[') {
        // 如果遇到新的{,递归调用进行嵌套匹配
        final nestedEndIndex = findMatchingClosingBracket(input, index);
        if (nestedEndIndex != -1) {
          index = nestedEndIndex; // 如果找到,跳到}的下一个
        } else {
          // 如果嵌套的{没有找到匹配的},跳过这个字符
          index++;
        }
      }
      index++;
    }

    return -1; // 没有找到匹配的
  }
  // 根据{或[找出对应的}或]
  String getClosingBracket(String openingBracket) {
    switch (openingBracket) {
      case '{':
        return '}';
      case '[':
        return ']';
      default:
        return '';
    }
  }

  /**
   *   残局自定义规则
   *   1. 先判断是否有王炸(两张大小王),有的话直接胜利
   *   2. 如果没有王炸,判断是否有四张相同的牌(四条),有的话直接胜利
   *   3. 如果都没有,判断是否有三张相同的牌(三条),有三条的胜利
   *   4. 如果都没有,判断是否有两对子(两组两张相同的牌),有两对子的胜利
   *   5. 如果都没有,判断最大的单张牌和次大的单张牌,最大的胜利
   *   6. 如果最大单张牌也一样,判断剩余的单张牌,较大的牌更多的一方胜利
   *   7. 如果一切都相同,平局
   */
  String solve(List<String> a, List<String> b) {
    // 判断王炸
    if (a.contains('joker') && b.contains('joker')) {
      return 'A';
    } else if (a.contains('joker')) {
      return 'A';
    } else if (b.contains('joker')) {
      return 'B';
    }
    // 判断四条
    if (checkFour(a) && !checkFour(b)) return 'A';
    if (checkFour(b) && !checkFour(a)) return 'B';

    // 判断三条
    if (checkThree(a) && !checkThree(b)) return 'A';
    if (checkThree(b) && !checkThree(a)) return 'B';

    // 判断两对子
    if (checkTwoPairs(a) && !checkTwoPairs(b)) return 'A';
    if (checkTwoPairs(b) && !checkTwoPairs(a)) return 'B';

    // 比较最大和次大单牌
    List<int> bigTwoA = getBigTwo(a);
    List<int> bigTwoB = getBigTwo(b);
    if (compareCards(bigTwoA, bigTwoB) > 0) return 'A';
    if (compareCards(bigTwoA, bigTwoB) < 0) return 'B';
    // 所有牌都一样,平局
    return 'DRAW';
  }

  // 判断四条
  bool checkFour(List<String> cards) {
    int len = cards.length;
    for (int i = 0; i < len - 3; i++) {
      if (cards[i] == cards[i + 1] &&
          cards[i + 1] == cards[i + 2] &&
          cards[i + 2] == cards[i + 3]) {
        return true;
      }
    }
    return false;
  }

// 判断三条
  bool checkThree(List<String> cards) {
    int len = cards.length;
    for (int i = 0; i < len - 2; i++) {
      if (cards[i] == cards[i + 1] && cards[i + 1] == cards[i + 2]) {
        return true;
      }
    }
    return false;
  }

// 判断两对子
  bool checkTwoPairs(List<String> cards) {
    int first = -1, second = -1;
    int len = cards.length;
    for (int i = 0; i < len - 1; i++) {
      if (cards[i] == cards[i + 1]) {
        if (first == -1) first = i;
        else if (second == -1) second = i;
        else return true;
      }
    }
    return second != -1;
  }

// 获取最大和次大单牌
  List<int> getBigTwo(List<String> cards) {
    List<int> bigTwo = cards.map((e) => getCardVal(e)).toList();
    bigTwo.sort();
    return bigTwo;
  }

// 比较两牌的大小
  int compareCards(List<int> a, List<int> b) {
    int aVal = a[0];
    int bVal = b[0];
    if (aVal != bVal) return aVal - bVal;
    return a[1] - b[1];
  }

  int getCardVal(String card) {
    if (card == 'joker') return 15;
    if (card == 'A') return 14;
    if (card == 'K') return 13;
    if (card == 'Q') return 12;
    if (card == 'J') return 11;
    return int.parse(card);
  }

  String getCardString(int card) {
    if (card == 15) return 'joker';
    if (card == 14) return 'A';
    if (card == 13) return 'K';
    if (card == 12) return 'Q';
    if (card == 11) return 'J';
    return card.toString();
  }
}